# lint

Image for linting in workflows

## Download image

```console
docker pull gitlab-registry.cern.ch/recast-atlas/images/lint:latest
```

## Use in workflows

Use the following pattern

```yaml
stages:
  - lint

pre-commit-check:
  stage: lint
  image: gitlab-registry.cern.ch/recast-atlas/images/lint:latest
  script:
    - pre-commit run --all-files
```
